﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceTask
{
    //TODO: Create public class 'Manager' here, which inherits from 'Employee' class

    //TODO: Define private integer field: 'quantity'

    //TODO: Define constructor with three parameters: 'name'(string), 'salary'(decimal) and 'clientAmount'(int). Assign two first parameters to base class.

    //TODO: Override public virtual method 'SetBonus', which increases bonus depending on clients amount

    public class Manager : Employee
    {
        private int quantity;
        public Manager(String name, decimal salary, int clientAmount) : base(name,salary)
        {
            quantity = clientAmount;
        }
        public override void SetBonus(decimal value)
        {
            if (quantity > 100 && quantity <= 200)
            {
                base.SetBonus(value + 500);
            }
            else if (quantity > 200)
            {
                base.SetBonus(value + 1000);
            }
        }

    }
}

