﻿using System;
using System.Collections.Generic;
using System.Text;


namespace InheritanceTask
{
    //TODO: Create public class 'Company' here

    //TODO: Define private field that is array of employees: 'employees'

    //TODO: Define constructor that gets array of employees, and assign them to its field

    //TODO: Define public method 'GiveEverbodyBonus' with parameter 'companyBonus', that set basic bonus to every employee in the company
    //TODO: Define public method 'TotalToPay', that returns the total salary + bonus of all employees of the company 
    //TODO: Define public method 'NameMaxSalary', that returns emloyee’s name who has maximum salary + bonus in the company
    public class Company
    {
        private Employee[] employees;
        public Company(Employee[] employees)
        {
            this.employees = employees;
        }
        public void GiveEverbodyBonus( decimal companyBonus)
        {
            foreach(Employee emp in employees)
            {
                emp.SetBonus(companyBonus);
            }
        }
        public decimal TotalToPay()
        {
            decimal total = 0;
            foreach(Employee emp in employees)
            {
                total += emp.ToPay();
            }
            return total;
        }
        public String NameMaxSalary()
        {
            decimal max = 0;
            String name = "";
            foreach(Employee emp in employees)
            {
                if(max < emp.ToPay())
                {
                    max = emp.ToPay();
                    name = emp.Name;
                }
            }
            return name;
        }

    }

}
