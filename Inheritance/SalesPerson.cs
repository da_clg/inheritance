﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceTask
{
    //TODO: Create public class 'SalesPerson' here, which inherits from 'Employee' class

    //TODO: Define private integer field: 'percent'

    //TODO: Define constructor with three parameters: 'name'(string), 'salary'(decimal) and 'percent'(int). Assign two first parameters to base class.

    //TODO: Override public virtual method 'SetBonus', which increases bonus depending on percent
    public class SalesPerson : Employee
    {
        private int percent;
        public SalesPerson(String name, decimal salary, int percent) : base(name, salary)
        {
            this.percent = percent;
        }
        public override void SetBonus(decimal value)
        {
            if(percent>100 && percent <= 200)
            {
                base.SetBonus(value * 2);
            }
            else if (percent > 200)
            {
                base.SetBonus(value * 3);
            }
        }
    }
}
